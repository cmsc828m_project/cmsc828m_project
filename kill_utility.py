# import mdptoolbox.example
import mdptoolbox
import numpy as np

prnt = True


def p(strng):
    if prnt:
        print strng


def remove_users_from_dict(user_dict, users_to_remove):
    """
    This is just a helper function to remove a list of users from a dictionary
    :param user_dict:
    :param users_to_remove:
    :return:
    """
    for user_id in users_to_remove:
        if user_id in user_dict:
            del user_dict[user_id]


def kill_user(user_dict):
    """
    This function simply updates the dying probability for the user
    :param user_dict: the user information dictionary
    :return: updated user_information_dict
    """

    fixed_multiplier = 0.01  # this is a random multiplier that is subtracted from the current prob. of dying with the
    # decay factor
    users_to_remove = set()
    for user_id, pair in user_dict.iteritems():
        for member, user_info in pair.iteritems():
            threshold_for_dying = np.random.uniform(low=0.0, high=1.0)
            current_probability_to_die = user_info['probability_to_die']
            if current_probability_to_die < threshold_for_dying:  # a user is chosen to die if the new probability is less
                # than the generated prob.
                # CASE: Survival
                decay_factor = user_info['decay_factor']
                new_probability_to_die = current_probability_to_die + (decay_factor * fixed_multiplier)
                user_info["probability_to_die"] = new_probability_to_die
            else:
                # CASE: Death
                users_to_remove.add(user_id)

    remove_users_from_dict(user_dict, users_to_remove)

    return user_dict, users_to_remove
