import gym


def play():
    env = gym.make("CartPole-v0")
    env.reset()
    for _ in range(1000):
        env.render()
        observation, reward, done, info_dict = env.step(env.action_space.sample())
        if done:
            print "done.."
            # break
            env.reset()

if __name__ == "__main__":
    play()