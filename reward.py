import numpy as np

# RWD:
# Urgency * ( 1-blood) * (1/1+age)

# for s1 in s:
#	for s2 in (s-s1):
#		compute RWD

ALPHA = 1.0
BETA = 1.0
prob_coming = 1.0
EPSILON = 0.01
INCOMPATIBILITY_PENALTY = 0.0
prnt = False


def P(strng):
    if prnt:
        print strng


def eval_blood(d, p):
    """
    A more relaxed way to evaluate the blood compatibility.
    :param d:
    :param p:
    :return:
    """
    if p % d == 0:
        return 1.0
    else:
        return INCOMPATIBILITY_PENALTY


# {'age':age, 'blood':btype[blood], 'decay_factor':df, 'probability_to_die':ptd}
def eval_donor_patient(donor, patient):
    """
    This function just computes the various donor patient matching parameters
    :param donor:
    :param patient:
    :return:
    """
    blood = eval_blood(patient['blood'], donor['blood'])  # Compatible == 1
    age_diff = abs(patient['age'] - donor['age'])
    urgency = patient['probability_to_die'] * donor['probability_to_die']
    return blood, age_diff, urgency


def reward_wait(donor, patient):
    """
    This function computes the reward function for the 'wait' action
    :param donor:
    :param patient:
    :return:
    """
    blood_compatible, age_diff, urgency = eval_donor_patient(donor, patient)  # Compatible == 1
    # (1 - 1/(1 + exp(-0.1 * x))) * 2
    # return blood_compatible * 1.0 / (1.0 + age_diff) * (ALPHA * urgency) * (BETA * prob_coming)
    return (1.0 - urgency) * (1.1 - blood_compatible)


def reward_transplant(donor, patient):
    """
    This function computes the reward function for the 'transplant' action
    :param donor:
    :param patient:
    :return:
    """
    blood_compatible, age_diff, urgency = eval_donor_patient(donor, patient)
    # age_multiplier = (1.0 - 1.0 / float(1.0 + np.exp(-0.1 * age_diff)))
    age_multiplier = (EPSILON + age_diff / 10)
    return blood_compatible / age_multiplier * (ALPHA * urgency)


def reward_wait_updated(donor, patient):
    blood_compatible, age_diff, urgency = eval_donor_patient(donor, patient)  # Compatible == 1
    # (1 - 1/(1 + exp(-0.1 * x))) * 2
    # return blood_compatible * 1.0 / (1.0 + age_diff) * (ALPHA * urgency) * (BETA * prob_coming)
    return (1.0 - urgency) * (1.0 - blood_compatible * (1.0 - EPSILON))


def reward_transplant_updated(donor, patient):
    blood_compatible, age_diff, urgency = eval_donor_patient(donor, patient)
    # age_multiplier = (1.0 - 1.0 / float(1.0 + np.exp(-0.1 * age_diff)))
    age_multiplier = ((1.0 + age_diff))
    return blood_compatible / age_multiplier * (ALPHA * urgency)


# TODO add into reward matrix hypothetical 'wish' matches
# TODO make sparse matrices for the reward functions
def fill_reward_matrix(user_dict):
    """
    This function fills in the reward matrix and also created the 3-Dimensional reward matrix which is fed into MDP Toolbox
    :param user_dict:
    :return:
    """
    R_wait = np.zeros((len(user_dict), len(user_dict)))
    R_transplant = np.zeros((len(user_dict), len(user_dict)))
    frm, to = 0, 0
    for donate_pair_id in user_dict:
        for recipient_pair_id in user_dict:
            if donate_pair_id != recipient_pair_id:
                R_wait[frm, to] = reward_wait(user_dict[donate_pair_id]['donor'],
                                              user_dict[recipient_pair_id]['patient'])
                R_transplant[frm, to] = reward_transplant(user_dict[donate_pair_id]['donor'],
                                                          user_dict[recipient_pair_id]['patient'])
            else:
                R_wait[frm, to] = R_wait[frm, to] = reward_wait(user_dict[donate_pair_id]['donor'],
                                                                user_dict[recipient_pair_id]['patient'])
                R_transplant[frm, to] = 0.0

            to += 1
        frm += 1
        to = 0

    P('wait')
    P(R_wait)
    P('transplant')
    P(R_transplant)
    R = np.array([R_wait, R_transplant])  # 3d array with two actions and the corresponding reward matrices
    # R = np.array([R_transplant, R_wait])  # 3d array with two actions and the corresponding reward matrices
    return R

# TODO Bellman function
