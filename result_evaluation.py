import matplotlib.pyplot as plt


def create_convergence_plot(convergence_value_list, number_of_states_list):
    print len(convergence_value_list)

    plt.plot([0.1035368447, 0.09046303534, 0.0271389106, 0.00814167318], label="4 states")
    plt.plot([0.9961315294, 0.8925520606, 0], label="2 states")
    plt.plot([0.9987846221, 0.3002635386, 0.1754166103, 0.1159359601, 0.07687667086, 0.05176669633, 0.03487265235, 0.02353270935, 0.01588104176, 0.01071938268], label="5 states")
    plt.plot([0.9987031491, 0.7285620567, 0.1956373403, 0.05138772558, 0.01386598314, 0.003732197163], label="11 states")
    plt.plot([0.9992771968, 0.8876855832, 0], label="9 states")
    plt.plot([0.9989703565, 0.4884488517, 0.2238721379, 0.1122133987, 0.05673397792, 0.02871802665, 0.01453826668, 0.007359986275], label="17 states")
    plt.ylabel("Variation values")
    plt.title("convergence dependence on the number of states at epsilon = 0.1")
    # for idx in range(0, len(convergence_value_list)):
    #     plt.plot(convergence_value_list[idx], label=str(number_of_states_list[idx]))

    plt.legend()

    plt.show()
