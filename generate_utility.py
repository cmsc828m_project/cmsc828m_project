
from numpy import random
import time
import numpy as np

random.seed(0)
COMING_PROBABILITY=0.9
DEATH_PROBABILITY=0.1
btype = {0: 1,  # O
         1: 2,  # A
         2: 3,  # B
         3: 6}  # AB
prnt = True


def p(strng):
    if prnt:
        print strng


def generate_user_features():
    """
    This function generates a user's age and bloodtype(int)
    :param user_dict: the user information dictionary
    :return: updated user_information_dict
    """
    age = random.randint(15, 80)
    blood = random.randint(4)
    df = random.uniform(low=0.0, high=1.0)
    ptd = random.uniform(low=0.0, high=DEATH_PROBABILITY)
    return {'age': age, 'blood': btype[blood], 'decay_factor': df, 'probability_to_die': ptd}


def generate_tuple():
    """
    This is a helper function to generate the donor and patient tuple
    :return:
    """
    return {'donor': generate_user_features(), 'patient': generate_user_features()}


def sample_user(user_dict):
    """
    This function adds agents to user dictionary with probability 0.5
    :param user_dict: the user information dictionary
    :return: updated user_information_dict
    """
    i = 1
    while random.random() < COMING_PROBABILITY:
        user_dict[str(time.time()) + str(i)] = generate_tuple()
        i += 1
    return user_dict


def add_users(user_dict, count):
    """
    Adds a fixed number of agents to user dictionary. Intended for initialization.
    :param user_dict: the user information dictionary
    :param count:	how many agents to add
    :return: updated user_information_dict
    """
    for i in range(count):
        user_dict[str(time.time()) + str(i)] = generate_tuple()
    return user_dict
