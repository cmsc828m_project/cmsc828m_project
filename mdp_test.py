import generate_utility as gen_util
import kill_utility as kill_util
import reward
import numpy as np
import my_mdp
import result_evaluation
from scipy import stats


INITIAL_STATES = 200
prnt = True


def P(strng):
    if prnt:
        print strng


def build_probability_matrix(user_dict):
    """
    This function generated the probability matrix for the current state of the system using the forest example.
    :param user_dict: user information dict
    :return: probability matrix
    """
    number_of_states = len(user_dict)
    # P( ('NUMBER OF STATES, ', number_of_states))

    # probability_matrix, _ = mdptoolbox.example.forest(S=number_of_states) # we do not need the hard-coded reward
    # matrix

    probability_matrix_a = np.empty((number_of_states, number_of_states))
    probability_matrix_a.fill(1.0 / float(number_of_states - 1.0))
    np.fill_diagonal(probability_matrix_a, 0.0)

    probability_matrix_b = np.zeros((number_of_states, number_of_states))
    np.fill_diagonal(probability_matrix_b, 1.0)

    return np.array([probability_matrix_b, probability_matrix_a])


def build_reward_matrix(user_dict):
    """
    This function computes the reward matrix based on the various parameters for a user.
    :param user_dict: user information dict
    :return: reward matrix
    """
    reward_matrix = reward.fill_reward_matrix(user_dict)
    return reward_matrix


def execute_relative_value_iteration(p, r, epsilon):
    """
    This function computes the value function for the relative Value Iteration algorithm
    :param p: probability matrix
    :param r: reward matrix
    :param epsilon: used for convergence threshold
    :return:
    """
    relative_value_iter = my_mdp.RelativeValueIteration(p, r, epsilon=epsilon)  # finds optimal
    relative_value_iter.run()

    print "sum of optimal value function for Relative VI: ", sum(relative_value_iter.V)


def execute_value_iteration_gs(p, r, gamma, epsilon):
    """
    This function computes the value function for the Gauss-Seidel Value Iteration algorithm
    :param p: probability matrix
    :param r: reward matrix
    :param epsilon: used for convergence threshold
    :return:
    """
    value_iter_gs = my_mdp.ValueIterationGS(p, r, discount=gamma, epsilon=epsilon)  # finds optimal
    value_iter_gs.run()

    print "sum of optimal value function for VI-GS: ", sum(value_iter_gs.V)


def check_value_iteration(user_dict, gamma=0.9):
    total_time_period = 10
    value_iter_list = []
    convergence_value_list = []
    number_of_states_for_convergence_comparison = []
    users_to_delete = []
    users_to_stay_ages = []
    users_to_delete_ages = []
    death_count = 0
    total_node_count = 0

    for i in range(0, total_time_period):
        # p, r = mdptoolbox.example.forest() # this is from the example to use the forest probabilities.
        old_users = user_dict.keys()
        total_node_count += len(old_users)
        user_dict, users_to_remove = update_user(user_dict)
        new_users = user_dict.keys()

        death_count += len(users_to_remove)

        p = build_probability_matrix(user_dict)
        r = build_reward_matrix(user_dict)

        '''
        this part of the code executes other possible VI algorithms i.e. Relative VI and VI Gauss-Seidel
        p1 = np.copy(p)
        p2 = np.copy(p)
        r1 = np.copy(r)
        r2 = np.copy(r)

        execute_relative_value_iteration(p1, r1, epsilon=0.1)
        execute_value_iteration_gs(p2, r2, gamma=gamma, epsilon=0.1)
        '''

        value_iter = my_mdp.ValueIteration(p, r, gamma, epsilon=0.1)  # finds optimal
        value_iter.setVerbose()
        value_iter.run()
        value_iter_list.append(value_iter)
        ep = value_iter.epsilon
        optimal_policy = value_iter.policy

        print "epsilon value: ", ep
        print "optimal policy: ", optimal_policy
        print "value function result: ", value_iter.V
        print "sum of optimal value function for VI: ", sum(value_iter.V)
        print "number of states: ", len(user_dict)
        print "Death Count: ", death_count
        print "Total Node Count: ", total_node_count
        death_count = 0
        total_node_count = 0

        convergence_value_list.append(value_iter.V)
        number_of_states_for_convergence_comparison.append(len(user_dict.keys()))

        for idx in range(0, len(optimal_policy)):
            if optimal_policy[idx] == 1:
                users_to_delete.append(user_dict.keys()[idx])

        for user_id in users_to_delete:
            if user_id in user_dict:
                users_to_delete_ages.append(user_dict[user_id]["donor"]["age"])
                users_to_delete_ages.append(user_dict[user_id]["patient"]["age"])
                del user_dict[user_id]  # remove the users that chose the action to transplant over the wait action.

        for user_id, info in user_dict.iteritems():
            if user_id in user_dict:
                users_to_stay_ages.append(user_dict[user_id]["donor"]["age"])
                users_to_stay_ages.append(user_dict[user_id]["patient"]["age"])

    result_evaluation.create_convergence_plot(convergence_value_list, number_of_states_for_convergence_comparison)

    deleted_age_stats = stats.describe(users_to_delete_ages)
    stay_age_stats = stats.describe(users_to_stay_ages)
    print deleted_age_stats
    print stay_age_stats


def update_user(user_dict):
    """

    :param user_dict:
    :return:
    """
    return kill_util.kill_user(gen_util.sample_user(user_dict))


def initialize_user_dict():
    """
    This function initializes the user dictionary. This is for bootstrapping the data
    :return:
    """
    user_dict = {}
    return gen_util.add_users(user_dict, INITIAL_STATES)


if __name__ == "__main__":
    user_dict = initialize_user_dict()
    check_value_iteration(user_dict, gamma=0.9)
